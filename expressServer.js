require('dotenv').config();
const path = require('path');
const cors = require('cors');
const express = require('express');
// const fs = require('fs');
// const morgan = require('morgan');
// const rfs = require('rotating-file-stream');

const app = express();
app.use(cors());

const publicPath = path.join(__dirname, 'build');

// app.use('/favicon.ico', express.static('./build/favicon.ico'));
app.use(express.static(publicPath));

app.get('*', (req, res) => {
  res.sendFile(path.join(publicPath, 'index.html'));
});

// const PORT = process.env.PORT || 80;

const PORT = 8080;
const HOST = '0.0.0.0';

app.listen(PORT, HOST);
