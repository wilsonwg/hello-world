FROM node:16

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

COPY build ./build

RUN yarn install --pure-lockfile
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source

RUN yarn global add nodemon --prefix /usr/local

# COPY build ./build
# COPY src/assets/img/favicon.ico ./build

#Copy node express server js file
COPY expressServer.js .

EXPOSE 8080

#Run the express server to serve the web content
CMD [ "yarn", "run", "web" ]